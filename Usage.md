# API-exercise

High Level Solution of this exercise
1. Implemented the API in python flask framework with MYSQl 8 as backend.
2. API and MYSQL will run in two separate containers.
3. For MYSQL - used a public image from docker hub.
4. For the API - Built a custom image from Python3 as base image.
   see [Dockerfile](./api//Dockerfile)
5. The complete solution gets deployed on k8s cluster.

## Local Testing
Assuming kubectl can connect to the cluster and the user deploying the solution is authorised to create objects.
Execute deploy.sh

## Automated Jenkins deploy
See [Jenkinsfile](./Jenkinsfile). The Jenkins file is deploying resources in two environments. The Jenkins job will trigger automatically on a Merge request.

## Future enhancements
- Add tests in the pipeline.
- Version the docker image properly.
- Peform security scan.
- Convert the deployment into Helm chart.
- Validate what is being changed in the source and only run the pipeline when the actual code is changing

