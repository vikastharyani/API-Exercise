import markdown
import os
import pymysql
import uuid
import traceback
import logging


# Import the framework
from flask import Flask, escape, request
from flask import jsonify
from werkzeug import generate_password_hash, check_password_hash
from flaskext.mysql import MySQL
from flask_uuid import FlaskUUID


# Create an instance of Flask
app = Flask(__name__)

# Create an instance of MYSQL
mysql = MySQL()

FlaskUUID(app)

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = os.environ['MYSQL_DATABASE_USER']
app.config['MYSQL_DATABASE_PASSWORD'] = os.environ['MYSQL_DATABASE_PASSWORD']
app.config['MYSQL_DATABASE_DB'] = os.environ['MYSQL_DATABASE_DB']
app.config['MYSQL_DATABASE_HOST'] = os.environ['MYSQL_SERVICE_SERVICE_HOST']

mysql.init_app(app)

# app route index will define here how the API works
@app.route('/')
def index():
    """Present some documentation"""

    # Open the API.md file
    with open(os.path.dirname(app.root_path) + '/API.md', 'r') as markdown_file:

        #Read the contents of markdown_file
        content = markdown_file.read()

        #Convert to HTML
        return markdown.markdown(content)


@app.route('/people', methods=['GET'])
def users():
    """Implementing the people API"""
    # Connect to MySQL and get data and return
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM passenger")
        rows = cursor.fetchall()
        # Initialize a passenger list
        passengerList = []
        jsondict = {}
        for row in rows:
            jsondict = {
            'uuid': str(uuid.uuid4()),
            'survived': bool(row[0]),
            'passengerClass': row[1],
            'name': row[2],
            'sex': row[3],
            'age': row[4],
            'siblingsOrSpousesAboard': row[5],
            'parentsOrChildrenAboard': row[6],
            'fare': float(row[7])
            }
            passengerList.append(jsondict)
            jsondict = {}

        resp = jsonify(passengerList)
        resp.status_code = 200
        return resp
    except Exception as e:
            logging.error(traceback.format_exc())
            return "{'message': 'Exception in handling request'}", 500

@app.route('/people', methods=['POST'])
def adduser():
    """Add user to Database"""
    try:
        if request.is_json:
            _json = request.get_json()
            _survived = _json['survived']
            _passengerClass = _json['passengerClass']
            _name = _json['name']
            _sex = _json['sex']
            _age = _json['age']
            _siblingsOrSpousesAboard = _json['siblingsOrSpousesAboard']
            _parentsOrChildrenAboard = _json['parentsOrChildrenAboard']
            _fare = _json['fare']

            # Check if JSON has all the values
            if type(_survived) and _passengerClass and _name and _sex and _siblingsOrSpousesAboard and type(_parentsOrChildrenAboard) and _fare:
                # save the record
                sql = "INSERT into passenger(survived, pclass, name, sex, age, `siblings/spouses aboard`, `parents/children aboard`, fare) VALUES(%s, %s, %s, %s, %s, %s, %s, %s)"
                data = (_survived, _passengerClass, _name, _sex, _age, _siblingsOrSpousesAboard, _parentsOrChildrenAboard, _fare,)
                conn = mysql.connect()
                cursor = conn.cursor()
                cursor.execute(sql, data)
                conn.commit()
                passengerList = []
                jsondict = {
                'uuid': str(uuid.uuid4()),
                'survived': _survived,
                'passengerClass': _passengerClass,
                'name': _name,
                'sex': _sex,
                'age': _age,
                'siblingsOrSpousesAboard': _siblingsOrSpousesAboard,
                'parentsOrChildrenAboard': _parentsOrChildrenAboard,
                'fare': _fare
                }
                passengerList.append(jsondict)
                resp = jsonify(passengerList)
                resp.status_code = 200
                return resp
            else:
                return {'message': 'Error in adding user'}, 500
        else:
            return {'message': 'Request was not json'}, 400

    except Exception as e:
            logging.error(traceback.format_exc())
            return "{'message': 'Exception in handling request'}", 500

@app.route('/people/<uuid(strict=False):id>/', methods=['GET'] )
def getinformation(id):
    """Get Information of a particular user"""
    try:
        if request.is_json:
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM passenger ORDER BY RAND() limit 1")
            row = cursor.fetchone()
            passengerList = []
            jsondict = {
            'uuid': id,
            'survived': bool(row[0]),
            'passengerClass': row[1],
            'name': row[2],
            'sex': row[3],
            'age': row[4],
            'siblingsOrSpousesAboard': row[5],
            'parentsOrChildrenAboard': row[6],
            'fare': row[7]
            }
            passengerList.append(jsondict)
            resp = jsonify(passengerList)
            resp.status_code = 200
            return resp
        else:
            return {'message': 'Request was not json'}, 400
    except Exception as e:
        logging.error(traceback.format_exc())
        return "{'message': 'Exception in handling request'}", 500

@app.route('/people/<uuid(strict=False):id>', methods=['DELETE'])
def Deleteinformation(id):
    """Delete Information of a user"""
    try:
        if request.is_json:
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("DELETE FROM passenger ORDER BY RAND() limit 1")
            conn.commit()
            return {'message': 'OK'}, 200
        else:
            return {'message': 'Request was not json'}, 400
    except Exception as e:
        logging.error(traceback.format_exc())
        return "{'message': 'Exception in handling request'}", 500

@app.route('/people/<uuid(strict=False):id>', methods=['PUT'])
def updateuser(id):
    """Update passenger to Database"""
    print ("In this method")
    try:
        print ("I am here")
        if request.is_json:
            _json = request.get_json()
            _survived = _json['survived']
            _passengerClass = _json['passengerClass']
            _name = _json['name']
            _sex = _json['sex']
            _age = _json['age']
            _siblingsOrSpousesAboard = _json['siblingsOrSpousesAboard']
            _parentsOrChildrenAboard = _json['parentsOrChildrenAboard']
            _fare = _json['fare']
            # return {'message': 'OK' }, 200
            # Validate the recieved values

            #if _survived and _passengerClass and _name and _sex and _siblingsOrSpousesAboard and _parentsOrChildrenAboard and _fare:
            if type(_survived) and _passengerClass and _name and _sex and _siblingsOrSpousesAboard and type(_parentsOrChildrenAboard) and _fare:
                # save the record
                sql = "UPDATE passenger SET survived=%s, pclass=%s, name=%s, sex=%s, age=%s, `siblings/spouses aboard`=%s, `parents/children aboard`=%s, fare=%s WHERE name=%s"
                data = (_survived, _passengerClass, _name, _sex, _age, _siblingsOrSpousesAboard, _parentsOrChildrenAboard, _fare, _name,)
                conn = mysql.connect()
                cursor = conn.cursor()
                cursor.execute(sql, data)
                print ("executed")
                conn.commit()
                return {'message': 'OK'}, 200
            else:
                return "Error in updating user", 400
        else:
            return {'message': 'Request was not json'}, 400

    except Exception as e:
            logging.error(traceback.format_exc())
            return "{'message': 'Exception in handling request'}", 500

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp
