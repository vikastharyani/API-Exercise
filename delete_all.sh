#!/bin/bash

# script to delete all kubernetes resources
echo -e "Deleting secret"
kubectl delete -f ./db/secret.yaml

echo -e "Deleting configmap"
kubectl delete configmap mysql-initdb

echo -e "Deleting MYSQL deployment"
kubectl delete -f ./db/mysql-deployment.yaml

echo -e "Deleting API serice"
kubectl delete -f ./api/api-deployment.yaml
