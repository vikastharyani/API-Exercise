#!/bin/bash

# This script assumes the k8s context is set in the terminal and the user is authorized
# to create resources in the K8s cluster.

# script to deploy all kubernetes resources

set -e

echo -e "Configuring MySQL"
kubectl apply -f $PWD/db/secret.yaml

echo -e "Initialize SQL"
kubectl create configmap mysql-initdb --from-file=./db/passenger.sql

echo -e "Deploying and initializing MYSQL"
kubectl apply -f ./db/mysql-deployment.yaml

# wait for mysql pod to be ready
while [[ $(kubectl get pods -l app=mysql -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do
  echo "waiting for mysql pod to be ready" && sleep 5;
done

echo -e "Deploying and configuring API serice"
kubectl apply -f ./api/api-deployment.yaml

export ip=`kubectl get services -l app=titanic-api -o jsonpath='{.items[0].metadata.annotations.field\.cattle\.io/publicEndpoints}' | jq .[0].addresses[0]`
export ipaddr=`echo "$ip" | sed -e 's/^"//' -e 's/"$//'`
export port=`kubectl get services -l app=titanic-api -o jsonpath='{.items[0].metadata.annotations.field\.cattle\.io/publicEndpoints}' | jq .[0].port`

echo "API endpoint http://$ipaddr:$port"
