#!/bin/bash

# script to deploy all kubernetes resources

set -e

if [ "$K8S_ENV" = "DEV" ]; then
  cluster="dev-usw2"
elif [[ "$K8S_ENV" = "PROD" ]]; then
  cluster="prod-usw2"
else
  echo "[ERROR] K8S_ENV is not set properly. Avaliable options are: DEV, STAGE, PROD"
  exit 1
fi

# set k8s config
echo "$K8S_CONFIG"  > kubeconfig
export KUBECONFIG="$PWD/kubeconfig"
echo "export KUBECONFIG='$PWD/kubeconfig'" >> env_var.sh
kubectl config set-context $cluster --namespace titanic
kubectl config use-context $cluster

echo -e "Configuring MySQL"
kubectl apply -f $PWD/db/secret.yaml

echo -e "Initialize SQL"
kubectl create configmap mysql-initdb --from-file=./db/passenger.sql --namespace titanic

echo -e "Deploying and initializing MYSQL"
kubectl apply -f ./db/mysql-deployment.yaml

# wait for mysql pod to be ready
while [[ $(kubectl get pods -l app=mysql -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do
  echo "waiting for mysql pod to be ready" && sleep 5;
done

echo -e "Deploying and configuring API serice"
kubectl apply -f ./api/api-deployment.yaml

export ip=`kubectl get services -l app=titanic-api -o jsonpath='{.items[0].metadata.annotations.field\.cattle\.io/publicEndpoints}' | jq .[0].addresses[0]`
export ipaddr=`echo "$ip" | sed -e 's/^"//' -e 's/"$//'`
export port=`kubectl get services -l app=titanic-api -o jsonpath='{.items[0].metadata.annotations.field\.cattle\.io/publicEndpoints}' | jq .[0].port`

echo "API endpoint http://$ipaddr:$port"
